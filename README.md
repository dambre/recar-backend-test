*Hi Mr/Mrs programmer,*

*We would like to congratulate You with passing the first stage of becoming a member of ReCar!*

This is the second stage where we will give you a coding task which will help us understand what your competences are.
We would like you to create vehicle parts API. It should be able to accept new part creation, existing part update, listing of created parts and deleting the parts you don't need anymore - basically we want you to create a simple CRUD.

Required functionality:

*  CRUD
*  Authentication

Required tech stack:

*  python3
*  Flask
*  SQLAlchemy (use any relational database)

Bonus tech:

*  GraphQL


Please upload your finished task to GIT (github/bitbucket) and provide us instructions on how to run this API. We would advice you to spend no more than 2-3 hours completing this task.

Good luck!
